# Wordpress Template

IMA template for Wordpress builds. Replace this with a description of the project.

Not much to see here, most of the instructions required for front end development are the README.md file which can be found in the root of the `ima-starter--twig` theme directory.

## docker

You'll need to setup docker before you get to theme development.

### Download the desktop application

The docker desktop application can be found [here](https://store.docker.com/editions/community/docker-ce-desktop-mac), you'll need a free account in order to download.


### SSL certificates

The certificate for each project is currently committed to the repository as Docker cannot access files outside its context. We are hoping to find a way round this, but in the meantime you will need to trust the certificate in Keychain for it to appear as valid in the URL bar.

### Project Configuration

The shared databases can be accessed without any further configuration. Should you need to access a database on your machine, instead of using 127.0.0.1 or localhost as your host, use:

`host.docker.internal`

You may also require a new MySQL user that can access the databases from any host name.

Also be aware that you will not be able to access local databases via phpMyAdmin when Docker is running as Apache cannot run at the same time. You will therefore require Sequel Pro.

### Docker Configuration

You should only need to change two values to get up and running.

The `hostname` value in `docker-compose.yml` and the `serverName` in `.docker/vhost.conf`

These should both be the URL used for local development.

### Build and run

**Ensure apache is not currently running on your machine**

From the root of the project directory, run

`$ docker-compose up`

Currently you can only run one Docker container at a time, so should you need to run a different project, hit Ctrl+C in the console and run the previous command from the root of your new project.

In the rare event you need to rebuild the container following changes to the Dockerfile, add the build flag to the previous command:

`docker-compose up --build`

### Setting up your theme

Duplicate the `ima-starter--twig` theme. Don't forget to change the theme name in `style.css` and activate it in the "appearance" section in the WordPress admin.

You can now move to the README in your new theme root.
