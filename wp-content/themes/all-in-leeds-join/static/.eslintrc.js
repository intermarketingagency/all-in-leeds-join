
module.exports = {
    "extends": "eslint:recommended",
    "rules" : {
        "indent": ["error", 4],
        "no-var": "error",
        "no-console" : "off",
        "no-irregular-whitespace" : "error",
        "no-mixed-spaces-and-tabs" : "error"
    },
    "globals": {
        "console" : false,
        "require" : false
    },
    "env": {
        "es6": true,
        "browser" : true
    },
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
    }
}
