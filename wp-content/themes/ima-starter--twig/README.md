
# The [ima](https://intermarketing.com) WordPress starter theme

A fork of the [Timber starter theme](https://github.com/timber/starter-theme)

## Recommended requirements
* PHP version 7.2
* MySQL version 5.6 or OR MariaDB version 10.0
* HTTPS support
* Node version 8
* Yarn version 1.9


## Getting started

Please make a copy of the starter theme and rename to something sensible for your project.

You will also need to update

* The `Theme Name` in style.css in the root of the theme directory.
* The path in the first argument of the `setPublicPath()` function in `static/webpack.config.js` to match your new theme directory name.


## File structure

`static/` is where you should keep your static front-end files e.g. your themes Sass files, JS files, fonts, and images.

`views/` contains all of your Twig templates. At the end of each PHP template file, you'll notice a `Timber::render()` function, the first parameter of this function requires a relative path to the Twig file where that data (or `$context`) will be used.

`bin/` and `tests/` ... don't worry about (or remove) these unless you know what they are and want to.

`build/` contains the current front end files bundled by webpack. These should be the only local files requested on the front end of the website, other than those added by plugins. Please note:

## Front end build
[Yarn](https://yarnpkg.com/en/) is the preferred package manager.

[webpack](https://webpack.js.org/) is used with [Encore](https://symfony.com/doc/current/frontend.html) for static asset management.

webpack utilises both [CSSnext](http://cssnext.io/) and [Babel](https://babeljs.io/) to transpile CSS and JavaScript respectively. Please make use of modern language features and use the [browserlist](https://github.com/browserslist/browserslist) declared in the package.json file to manage compatibility.

#### Installation

From the `static/` directory, run `$ yarn install`

#### Building for development
From the `static/` directory, run `$ yarn watch`. This will poll for changes to files and generate a build in the `static/build/` directory accordingly. The directory will **not** be wiped before each build (This is a [Known issue](https://github.com/symfony/webpack-encore/issues/162)). Assets such as JavaScript, and CSS will be **un-minified**.


#### Building for production
From the `static/` directory, run `$ yarn build`. This will generate a build in the `static/build/` directory. The directory will be wiped before each build. Assets such as JavaScript, and CSS will be **minified**. Production builds should only be created in local development environments for testing/debugging purposes. In production environments, an appropriate build pipeline should be used. We've included a boiler-plate pipeline configuration file for [BitBucket](https://bitbucket.org/) in the root of the project directory.

#### Static assets

As part of the build process webpack creates a file named `manifest.json`, which contains references to all static assets used in the theme. In order to return paths to 'versioned' assets (i.e `rob.123234554.jpg`), please use the `asset()` function documented in `functions/ima-timber-theme.php`.

##### PHP

`echo asset('app.js')`

##### Twig

`{{ asset('app.js') }} `


## Other Resources

The [main Timber Wiki](https://github.com/jarednova/timber/wiki) is great, so reference those often. Also, check out these articles and projects for more info:

* [Twig for Timber Cheatsheet](http://notlaura.com/the-twig-for-timber-cheatsheet/)
* [Timber and Twig Reignited My Love for WordPress](https://css-tricks.com/timber-and-twig-reignited-my-love-for-wordpress/) on CSS-Tricks
* [Timber Video Tutorials](https://github.com/jarednova/timber/wiki/Video-Tutorials) and [an incomplete set of screencasts](https://www.youtube.com/playlist?list=PLuIlodXmVQ6pkqWyR6mtQ5gQZ6BrnuFx-) for building a Timber theme from scratch.
