// webpack.config.js
const Encore = require('@symfony/webpack-encore');

Encore


    // the project directory where all compiled assets will be stored
    .setOutputPath('./build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/wp-content/themes/ima-starter--twig/static/build')

    // will create public/build/app.js and public/build/app.css
    .addEntry('app', './js/app.js')
    .addEntry('test-route', './js/route.js')
    .enableSingleRuntimeChunk()
    .enableEslintLoader()

    // allow legacy applications to use $/jQuery as a global variable
    // .autoProvidejQuery()

    // enable source maps during development
    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning()

    // allow sass/scss files to be processed
    .enableSassLoader()

    .enablePostCssLoader()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();
