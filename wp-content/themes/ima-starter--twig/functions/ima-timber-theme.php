<?php
/**
 * Check Timber plugin has been activated
 **/
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

/**
 * Set timber templates directory
 **/
Timber::$dirname = array('templates', 'views');

/**
 * Theme
 **/
class imaSite extends TimberSite {

	private $manifest;

	function __construct() {
        $this->setup_theme_capabilities();
        $this->manage_robots();
        $this->manage_timber();
        if(!is_admin()) {
            $this->manage_scripts();
            $this->cleanup_head();
        }
        if(function_exists('acf_add_options_sub_page')) {
            acf_add_options_page();
        }
        parent::__construct();
	}

    /**
     * Configures the themes capabilities
     **/
	function setup_theme_capabilities() {
    	add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
	}

	/**
     * Encueues the themes scripts
     **/
	function manage_scripts () {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ), 11);
    }

	/**
     * Manages the themes robot.txt file
     **/
	function manage_robots () {
    	add_filter( 'robots_txt', array( $this, 'remove_admin_from_robots' ) );
	}

	/**
     * Add to timber context or custom functions to twig
     **/
	function manage_timber () {
        add_filter( 'timber_context', array( $this, 'add_to_timber_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
	}

	/**
     * Removes head scripts that are not required
     **/
	function cleanup_head () {
        add_filter('ima_wp_head', array( $this, 'remove_yoast_comments' ), 10, 1);
	}


    /**
     * Set timber templates directory
     * @return array
     * $context['foo'] = 'bar';
	 * $context['stuff'] = 'I am a value set in your functions.php file';
	 * $context['notes'] = 'These values are available everytime you call Timber::get_context();';
     **/
	function add_to_timber_context( $context ) {
		$context['menu'] = new TimberMenu();
		$context['options'] = get_fields('options');
		$context['site'] = $this;
		return $context;
	}

    /**
     * Add custom functions to twig
     * @return obj
     **/
	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension(new Twig_Extension_StringLoader() );
		$twig->addFunction(new Twig_SimpleFunction('ima_wp_head', array( $this, 'ima_wp_head') ) );
		$twig->addFunction(new Twig_SimpleFunction('asset', array( $this, 'asset') ) );
		return $twig;
	}

    /**
     * Replace specific strings within the output of wp_head()
     **/
    function ima_wp_head() {
        ob_start();
        wp_head();
        $head = ob_get_clean();
        $head = apply_filters('ima_wp_head', $head);
        return $head;
    }

	/**
	 * Returns a versioned asset URL form the manifest
	 * @return string
	 **/
	function asset($buildPath) {
		$key = ltrim(parse_url(get_template_directory_uri() . '/static/build/' . $buildPath, PHP_URL_PATH), '/');
		if (!$this->manifest) {
			$this->manifest = json_decode(file_get_contents(get_stylesheet_directory() . '/static/build/manifest.json'));
		}
		if (isset($this->manifest->$key)) {
			$assetPath = $this->manifest->$key;
		} else {
			throw new Exception($key . ' does not exist in the manifest.json file');
		}
		return $assetPath;
	}

	/**
     * Enqueues the themes CSS + JS files
     **/
    function enqueue_assets() {
		wp_enqueue_script( 'runtime', $this->asset('runtime.js'), null, null, true );
		wp_enqueue_script( 'app', $this->asset('app.js'), null, null, true );
		wp_enqueue_style( 'app', $this->asset('app.css'), null, true );
    }


	/**
     * Remove wp admin from robots.txt
     */
    function remove_admin_from_robots( $output ) {
        $output = preg_replace( '/^Disallow: .*?\/wp-admin\/$/m', '', $output );
        return $output;
    }

    /**
     * Remove Yoast comments
     */
    function remove_yoast_comments( $head ) {
        $head = preg_replace( '/<!-- This site is optimized with the Yoast SEO plugin .*? -->/i', '', $head );
        $head = str_replace( '<!-- / Yoast SEO plugin. -->', '', $head );
        $head = preg_replace( '/<!-- This site uses the Google Analytics by Yoast plugin version .*? -->/i', '', $head );
        $head = str_replace( '<!-- / Google Analytics by Yoast -->', '', $head );

        return $head;
    }

}

new imaSite();
