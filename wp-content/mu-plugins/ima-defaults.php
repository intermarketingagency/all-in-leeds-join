<?php
/*
Plugin Name: Intermarketing Defaults
Plugin URI: http://intermarketing.com
Description: Applies default styling and functionality for all WordPress builds
Version: 1
*/

if (is_admin()) {


    // Creates new sort option in 'Media' (e.g. .pdf)
    function ima_add_post_mime_type($post_mime_types)
    {
        $post_mime_types['application/pdf'] = array(
            __('PDF'),
            __('Manage PDF'),
            _n_noop('PDF <span class="count">(%s)</span>', 'PDF <span class="count">(%s)</span>')
        );
        return $post_mime_types;
    }
    add_filter('post_mime_types', 'ima_add_post_mime_type');

    // Hides Dashboard items
    function ima_dashboard_widgets()
    {
        global $wp_meta_boxes;
        unset(
            $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
            $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
            $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']
        );
    }
    add_action('wp_dashboard_setup', 'ima_dashboard_widgets');

    // Fix chrome bug
    function chromefix_inline_css()
    {
        wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0); }' );
    }
    add_action('admin_enqueue_scripts', 'chromefix_inline_css');

}

/**
 * Remove <p> from around images
 * @param string $content
 * @return string
 */
function filter_ptags_on_images($content)
{
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
